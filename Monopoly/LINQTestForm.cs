﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Monopoly
{
    public partial class LINQTestForm : Form
    {
        LINQTest test = new LINQTest();
        public LINQTestForm()
        {
            InitializeComponent();
        }

        private void testButton_Click(object sender, EventArgs e)
        {
            valuesListBox.Items.Add(test.Show());
            valuesListBox.Items.Add("Maxim: " + test.Maxim());
            valuesListBox.Items.Add("MaximForEach: " + test.MaximForEach());
            valuesListBox.Items.Add("MaximLINQ: " + test.MaximLINQ());

            test.Sort();
            valuesListBox.Items.Add("Sorted:" + test.Show());
           test.InversSort();
            valuesListBox.Items.Add("Inversat:" + test.Show());

        }

        private void cellTestsButton_Click(object sender, EventArgs e)
        {
            LINQTestObjects t = new LINQTestObjects();
            valuesListBox.Items.Add(t.Show());
            valuesListBox.Items.Add("Maxim: " + t.Maxim());
            valuesListBox.Items.Add("MaximLINQ: " + t.MaximLINQ());

            t.Sort();
           valuesListBox.Items.Add("Sorted:" + t.Show());

           t.RandomizeList();
           valuesListBox.Items.Add("Ghiveci:" + t.Show());

          t.SortByBuyout();
            valuesListBox.Items.Add("Dupa pret:" + t.Show());

            valuesListBox.Items.Add("Suma totala:" + t.SumOfBuyoutPrices());

            valuesListBox.Items.Add("Alea rosii:" + t.StringCuAleaRosii());
            valuesListBox.Items.Add("Alea scumpe:" + t.StringCuAleaScumpe(200));


        }
    }
}
