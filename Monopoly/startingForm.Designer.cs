﻿namespace Monopoly
{
    partial class startingForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(startingForm));
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.startButton = new System.Windows.Forms.Button();
            this.panel65 = new System.Windows.Forms.Panel();
            this.panel66 = new System.Windows.Forms.Panel();
            this.label33 = new System.Windows.Forms.Label();
            this.panel67 = new System.Windows.Forms.Panel();
            this.label34 = new System.Windows.Forms.Label();
            this.panel68 = new System.Windows.Forms.Panel();
            this.dicePanel = new System.Windows.Forms.Panel();
            this.diceLabel = new System.Windows.Forms.Label();
            this.playerNameLabel = new System.Windows.Forms.Label();
            this.playerMoneyLabel = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.playerPositionLabel = new System.Windows.Forms.Label();
            this.nextTurnButton = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panel65.SuspendLayout();
            this.panel66.SuspendLayout();
            this.panel67.SuspendLayout();
            this.dicePanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(317, 414);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(820, 687);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // startButton
            // 
            this.startButton.Location = new System.Drawing.Point(1230, 307);
            this.startButton.Margin = new System.Windows.Forms.Padding(4);
            this.startButton.Name = "startButton";
            this.startButton.Size = new System.Drawing.Size(180, 46);
            this.startButton.TabIndex = 1;
            this.startButton.Text = "START";
            this.startButton.UseVisualStyleBackColor = true;
            this.startButton.Click += new System.EventHandler(this.startButton_Click);
            // 
            // panel65
            // 
            this.panel65.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.panel65.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel65.Controls.Add(this.panel66);
            this.panel65.Controls.Add(this.panel67);
            this.panel65.Controls.Add(this.panel68);
            this.panel65.Location = new System.Drawing.Point(776, 577);
            this.panel65.Margin = new System.Windows.Forms.Padding(4);
            this.panel65.Name = "panel65";
            this.panel65.Size = new System.Drawing.Size(220, 306);
            this.panel65.TabIndex = 18;
            // 
            // panel66
            // 
            this.panel66.BackColor = System.Drawing.Color.White;
            this.panel66.Controls.Add(this.label33);
            this.panel66.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel66.Location = new System.Drawing.Point(0, 156);
            this.panel66.Margin = new System.Windows.Forms.Padding(4);
            this.panel66.Name = "panel66";
            this.panel66.Size = new System.Drawing.Size(216, 146);
            this.panel66.TabIndex = 3;
            // 
            // label33
            // 
            this.label33.BackColor = System.Drawing.Color.White;
            this.label33.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.label33.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label33.Location = new System.Drawing.Point(0, 40);
            this.label33.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label33.Name = "label33";
            this.label33.Padding = new System.Windows.Forms.Padding(2);
            this.label33.Size = new System.Drawing.Size(216, 106);
            this.label33.TabIndex = 1;
            this.label33.Text = "$200";
            this.label33.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel67
            // 
            this.panel67.BackColor = System.Drawing.Color.Cyan;
            this.panel67.Controls.Add(this.label34);
            this.panel67.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel67.Location = new System.Drawing.Point(0, 50);
            this.panel67.Margin = new System.Windows.Forms.Padding(4);
            this.panel67.Name = "panel67";
            this.panel67.Size = new System.Drawing.Size(216, 106);
            this.panel67.TabIndex = 2;
            // 
            // label34
            // 
            this.label34.BackColor = System.Drawing.Color.White;
            this.label34.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label34.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label34.Location = new System.Drawing.Point(0, 0);
            this.label34.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label34.Name = "label34";
            this.label34.Padding = new System.Windows.Forms.Padding(2);
            this.label34.Size = new System.Drawing.Size(216, 106);
            this.label34.TabIndex = 0;
            this.label34.Text = "Inginerie si litere";
            this.label34.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel68
            // 
            this.panel68.BackColor = System.Drawing.Color.PaleTurquoise;
            this.panel68.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel68.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel68.Location = new System.Drawing.Point(0, 0);
            this.panel68.Margin = new System.Windows.Forms.Padding(4);
            this.panel68.Name = "panel68";
            this.panel68.Size = new System.Drawing.Size(216, 50);
            this.panel68.TabIndex = 1;
            // 
            // dicePanel
            // 
            this.dicePanel.BackColor = System.Drawing.Color.White;
            this.dicePanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.dicePanel.Controls.Add(this.diceLabel);
            this.dicePanel.Location = new System.Drawing.Point(276, 259);
            this.dicePanel.Name = "dicePanel";
            this.dicePanel.Size = new System.Drawing.Size(96, 94);
            this.dicePanel.TabIndex = 19;
            // 
            // diceLabel
            // 
            this.diceLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.diceLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.diceLabel.Location = new System.Drawing.Point(0, 0);
            this.diceLabel.Name = "diceLabel";
            this.diceLabel.Size = new System.Drawing.Size(94, 92);
            this.diceLabel.TabIndex = 0;
            this.diceLabel.Text = "5";
            this.diceLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // playerNameLabel
            // 
            this.playerNameLabel.AutoSize = true;
            this.playerNameLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.playerNameLabel.Location = new System.Drawing.Point(540, 133);
            this.playerNameLabel.Name = "playerNameLabel";
            this.playerNameLabel.Size = new System.Drawing.Size(130, 51);
            this.playerNameLabel.TabIndex = 20;
            this.playerNameLabel.Text = "name";
            // 
            // playerMoneyLabel
            // 
            this.playerMoneyLabel.AutoSize = true;
            this.playerMoneyLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.playerMoneyLabel.Location = new System.Drawing.Point(589, 201);
            this.playerMoneyLabel.Name = "playerMoneyLabel";
            this.playerMoneyLabel.Size = new System.Drawing.Size(94, 51);
            this.playerMoneyLabel.TabIndex = 21;
            this.playerMoneyLabel.Text = "100";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(545, 201);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(46, 51);
            this.label1.TabIndex = 22;
            this.label1.Text = "$";
            // 
            // playerPositionLabel
            // 
            this.playerPositionLabel.AutoSize = true;
            this.playerPositionLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.playerPositionLabel.Location = new System.Drawing.Point(545, 278);
            this.playerPositionLabel.Name = "playerPositionLabel";
            this.playerPositionLabel.Size = new System.Drawing.Size(46, 51);
            this.playerPositionLabel.TabIndex = 23;
            this.playerPositionLabel.Text = "0";
            // 
            // nextTurnButton
            // 
            this.nextTurnButton.Location = new System.Drawing.Point(666, 307);
            this.nextTurnButton.Name = "nextTurnButton";
            this.nextTurnButton.Size = new System.Drawing.Size(191, 92);
            this.nextTurnButton.TabIndex = 24;
            this.nextTurnButton.Text = "ROLL";
            this.nextTurnButton.UseVisualStyleBackColor = true;
            this.nextTurnButton.Click += new System.EventHandler(this.nextTurnButton_Click);
            // 
            // startingForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1668, 1560);
            this.Controls.Add(this.nextTurnButton);
            this.Controls.Add(this.playerPositionLabel);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.playerMoneyLabel);
            this.Controls.Add(this.playerNameLabel);
            this.Controls.Add(this.dicePanel);
            this.Controls.Add(this.panel65);
            this.Controls.Add(this.startButton);
            this.Controls.Add(this.pictureBox1);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "startingForm";
            this.Text = "MONOPOLY";
            this.Load += new System.EventHandler(this.startingForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panel65.ResumeLayout(false);
            this.panel66.ResumeLayout(false);
            this.panel67.ResumeLayout(false);
            this.dicePanel.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button startButton;
        private System.Windows.Forms.Panel panel65;
        private System.Windows.Forms.Panel panel66;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Panel panel67;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Panel panel68;
        private System.Windows.Forms.Panel dicePanel;
        private System.Windows.Forms.Label diceLabel;
        private System.Windows.Forms.Label playerNameLabel;
        private System.Windows.Forms.Label playerMoneyLabel;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label playerPositionLabel;
        private System.Windows.Forms.Button nextTurnButton;
    }
}

