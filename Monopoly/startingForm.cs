﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Monopoly
{
    public partial class startingForm : Form
    {
        MonopolyGame game;
        public startingForm()
        {
            InitializeComponent();
        }

        private void startButton_Click(object sender, EventArgs e)
        {
            game = new MonopolyGame();
            game.Start(this);

        }

        internal void HidePlayer(Player player)
        {
           game.board.cellList[player.position].
                colorPanel.Controls.Clear();
        }

        internal void ShowPlayer(Player player)
        {
            playerNameLabel.Text = player.name;
            playerMoneyLabel.Text = player.money.ToString();
            playerPositionLabel.Text = player.position.ToString();

            game.board.cellList[player.position].
                colorPanel.Controls.Add(new Label { Text = player.name });
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void startingForm_Load(object sender, EventArgs e)
        {

          
        }

        private void nextTurnButton_Click(object sender, EventArgs e)
        {
            game.NextTurn();
        }
    }
}
