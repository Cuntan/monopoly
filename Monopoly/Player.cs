﻿namespace Monopoly
{
    internal class Player
    {
        public string name;
        public int money;
        public int position;

        public Player() {
            name = "Default";
            money = 1000;
            position = 0;
        }
     public Player(string name, int money) {
            this.name = name;
            this.money = money;
            position = 0;
        }

        public void Show(startingForm f) {
            f.ShowPlayer(this);
        }

    }
}