﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Windows.Forms;

namespace Monopoly
{
    internal class MonopolyGame
    {
        public Board board = new Board();
        public List<Player> players = new List<Player>();
        public Dice dice = new Dice();
        public startingForm f;
        int currentPlayerIndex = 0;
        internal void Start(startingForm f)
        {
            players.Add(new Player { name = "Dorel", money = 1000, position = 0 });
            players.Add(new Player { name = "Toni", money = 1000, position = 0 });

            board.Show(f);

            this.f = f;
            currentPlayerIndex = 0;

            //PlayGame();
        }

        private void PlayGame()
        {
            int currentPlayerIndex = 0;
            Player player = players[currentPlayerIndex];

            while (!Winner(player))
            {
                int dice = Dice.Roll();
                HidePlayer(player);
                UpdatePlayer(player,dice);
                ShowPlayer(player);

                currentPlayerIndex = NextPlayerIndex(currentPlayerIndex);
                player = players[currentPlayerIndex];

                f.Refresh();
                Thread.Sleep(1000);
            }
        }

        private int NextPlayerIndex(int currentPlayerIndex)
        {
            currentPlayerIndex++;
            return  currentPlayerIndex % players.Count;
        }

        internal void NextTurn()
        {
            
            Player player = players[currentPlayerIndex];

            if (!Winner(player))
            {
                int dice = Dice.Roll();
                HidePlayer(player);
                UpdatePlayer(player, dice);
                // PlayerActions();
                ShowPlayer(player);

                currentPlayerIndex = NextPlayerIndex(currentPlayerIndex);
                
                f.Refresh();
                Thread.Sleep(1000);
            }
        }

        private void ShowPlayer(Player player)
        {
            f.ShowPlayer(player);
        }

        private void UpdatePlayer(Player player, int dice)
        {
            player.position += dice;
            if (player.position >= 40)
            {
                player.money += Constants.MoneyRaiseOnPassingStart;
            }
            player.position %= 40;
        }

        private void HidePlayer(Player player)
        {
            f.HidePlayer(player);
        }

        private bool Winner(Player player)
        {
            return players.Where(x => x.money > 0).Count() == 1; // un singur jucator a ramas cu suma peste 0
        }
    }
}