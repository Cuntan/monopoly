﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Monopoly
{
    class LINQTest
    {
        List<int> a = new List<int>() { 3, 4, 5, 9, 8, 7, 2 };


        public int Maxim()
        {
            int mx = -10000;
            for(int i = 0; i < a.Count; i++)
            {
                if (a[i] > mx)
                {
                    mx = a[i];
                }
            }
            return mx;
        }

   public int MaximForEach()
        {
            int mx = -10000;
            foreach(int x in a)
            {
                if (x > mx)
                {
                    mx = x;
                }
            }
            return mx;
        }

        internal int MaximLINQ()
        {
            return a.Max();
        }
        public void Sort()
        {
            a.Sort();
        }
        public void InversSort()
        {
            a.Sort();
            a.Reverse();
        }


        public string Show()
        {
            string output="";
            foreach(int x in a)
            {
                output += (" " + x);
            }

            return output;
        }
    }
}
